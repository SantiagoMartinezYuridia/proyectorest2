<?php

$app->get('/', function() use ($app_response) {
    
});

$app->get('/entidades', function() use ($app_response, $app_request) {
    $xml = formatos($app_request, $app_response);
    conect();
    $datos = getEntidades(fields($app_request, "entidad"), filter($app_request), order_by($app_request), limit($app_request));
    $status = 404;
    if ($datos) {
        $status = 200;
    }
    respuesta($app_response, $status, "Entidad", $datos, $xml);
});

$app->get('/entidades/:id', function($id) use($app_request, $app_response) {
    $xml = formatos($app_request, $app_response);
    conect();
    $datos = array();
    $probar = isIdEntidad($id);
    if (is_bool($probar)) {
        $datos = getEntidad($id, fields($app_request, "entidad"));
		$status = 404;
        if ($datos) {
            $status = 200;
        }
    } else{
    $status = 400;}
	respuesta($app_response, $status, "Entidad", $datos, $xml);
});



$app->get('/entidades/:id/municipios', function($id) use($app_request, $app_response) {
    $xml = formatos($app_request, $app_response);
    conect();
    $datos = array();
    $validId = isIdEntidad($id, "Municipio");
    if (is_bool($validId)) {
        $datos = getMunicipios($id, fields($app_request, "municipio"), filter($app_request), order_by($app_request), limit($app_request));
		$status = 404;
        if ($datos) {
            $status = 200;
        }
    } else{
    $status = 400;}
		    respuesta($app_response, $status, "Municipio", $datos, $xml);

    }
);

$app->get('/entidades/:id/municipios/:mun', function($id, $mun) use($app_request, $app_response) {
    $xml = formatos($app_request, $app_response);
    conect();
	$datos = array();
	 $validId = isIdEntidad($mun, "Municipio");
	if (is_bool($validId)) {
    $datos = getMunicipio($id, $mun, fields($app_request, "municipio"));
    $status = 404;
        if ($datos) {
            $status = 200;
        }
    } else{
    $status = 400;}
    respuesta($app_response, $status, "Municipio", $datos, $xml);
});


$app->get('/entidades/:id/municipios/:mun/indicadores', function($id, $mun) use($app_request, $app_response) {
    $xml = formatos($app_request, $app_response);
    conect();
    $datos = getIndicadores($id, $mun, fields($app_request, "indicador"), filter($app_request), order_by($app_request), limit($app_request));
    $status = 404;
    if ($datos) {
        $status = 200;
    } else {
        $datos = array();
    }
    respuesta($app_response, $status, "Indicador", $datos, $xml);
});

$app->get('/entidades/:id/municipios/:mun/indicadores/:ind', function($id, $mun, $ind) use($app_request, $app_response) {
    $xml = formatos($app_request, $app_response);
    conect();
    $datos = getIndicador($id, $mun, $ind, -1, fields($app_request, "indicador"), filter($app_request), order_by($app_request), limit($app_request));
    $status = 404;
    if ($datos) {
        $status = 200;
    } else {
        $datos = array();
    }
    respuesta($app_response, $status, "Indicador", $datos, $xml);
});

$app->get('/entidades/:id/municipios/:mun/indicadores/:ind/:anio', function($id, $mun, $ind, $anio) use($app_request, $app_response) {
    $xml = formatos($app_request, $app_response);
    conect();
    $datos = getIndicador($id, $mun, $ind, $anio, fields($app_request, "valor inner join indicador"));
    $status = 404;
    if ($datos) {
        $status = 200;
    } else {
        $datos = array();
    }
    respuesta($app_response, $status, "Valor", $datos, $xml);
});
?>