<?php

function respuesta($app_response, $status, $tipo, $data, $format = 'json') {
    if (!$data)
        $data = array();
    if ($format == 'json') {
        json_response($tipo, $app_response, $status, $data);
    } else if ($format == 'xml') {
        xml_response($app_response, $status, $tipo, $data);
    } else {
        $app_response->status(400);
    }
}

function json_response($tipo, $app_response = null, $status = 200, $data = array()) {
    $app_response['Content-Type'] = 'application/json';
    $app_response->status($status);
    $resultado = $data;

    if (isset($_GET['callback'])) { // Si es una petición cross-domain
        $app_response->body($_GET['callback'] . '(' . generarJSON($resultado,$tipo) . ')');
    } else {
        //$app_response->body(json_encode($resultado));
        $app_response->body(generarJSON($resultado, $tipo));
    }
}

function xml_response($app_response = null, $status = 200, $tipo = "data", $data = array()) {
    $app_response['Content-Type'] = 'application/xml';
    $app_response->status($status);
    $resultado = new DOMDocument('1.0', 'UTF-8');
    $xmlRaiz = $resultado->createElement('xml');
    $xmlRaiz = $resultado->appendChild($xmlRaiz);
    if (is_array($data)) {
        foreach ($data as $i => $v) {
            $row = $resultado->createElement($tipo);
            if (is_array($data[$i])) {
                foreach ($data[$i] as $clave => $valor) {
                    $nodo = $resultado->createElement($clave);
                    $nodo = $row->appendChild($nodo);
                    $nodo->nodeValue = ($valor);
                }
            } else {
                foreach ($data as $clave => $valor) {
                    $nodo = $resultado->createElement($clave);
                    $nodo = $row->appendChild($nodo);
                    $nodo->nodeValue = ($valor);
                }
            }
            $xmlRaiz->appendChild($row);
        }
    } else {
        $row = $resultado->createElement($tipo);
        foreach ($data as $clave => $valor) {
            $nodo = $resultado->createElement($clave);
            $nodo = $row->appendChild($nodo);
            $nodo->nodeValue = ($valor);
        }
        $xmlRaiz->appendChild($row);
    }
    $app_response->body($resultado->saveXML());
}

function generarJSON($array, $tipo) {
    $rest = "{";
    if (is_array($array)) {
        foreach ($array as $key => $value) {
            $rest.= '"' . $tipo . '_' . $key . '":{';
            foreach ($array[$key] as $key2 => $value2) {
                if (is_numeric($value2))
                    $rest.='"' . $key2 . '":' . $value2 . ',';
                else {
                    $rest.='"' . $key2 . '":"' . $value2 . '",';
                }
            }

            $rest = substr($rest, 0, sizeof(str_split($rest)) - 1);
            $rest.="},";
        }

        $rest = substr($rest, 0, sizeof(str_split($rest)) - 1);
        
    } else {
        $obj = (array) $array;
        $rest.= '"' . $tipo . '":{';
        foreach ($obj as $key2 => $value2) {
            if (is_numeric($value2))
                $rest.='"' . $key2 . '":' . $value2 . ',';
            else {
                $rest.='"' . $key2 . '":"' . $value2 . '",';
            }
        }
        $rest = substr($rest, 0, sizeof(str_split($rest)) - 1);
        $rest.="}";
    }
    if($array)
        $rest.="}";
    return $rest;
}

?>